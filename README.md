# pyhton_qp_examples


Velocity, acceleration and torque qp implentation in python using piniocchio.


## Install anaconda env
```
conda env create -f env.yaml
conda activate pyqp_examples
```

## pinocchio robot wrapper
Pyhton package for simpler manipulaiton of the pinocchio library
```
pip install git+https://gitlab.inria.fr/auctus-team/people/antunskuric/pynocchio.git
```

## Run the examples
```
jupyter lab
```